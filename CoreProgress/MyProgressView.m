//
//  MyProgressView.m
//  DynamicChart
//
//  Created by dg11185_zal on 14-10-24.
//  Copyright (c) 2014年 dg11185. All rights reserved.
//

#import "MyProgressView.h"

@implementation MyProgressView

@synthesize progress,progressWidth;
@synthesize bgColor,progressColor;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //创建并添加底图图层
        bgLayer = [CAShapeLayer new];
        bgLayer.fillColor = nil;
        bgLayer.frame = self.bounds;
        [self.layer addSublayer:bgLayer];
        
        //创建并添加进度图图层
        progressLayer = [CAShapeLayer new];
        progressLayer.fillColor = nil;
        progressLayer.frame = self.bounds;
        progressLayer.lineCap = kCALineCapRound;
        [self.layer addSublayer:progressLayer];
        
        //进度条宽度，默认5
        self.progressWidth = 5;
        progressLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.progressWidth, self.frame.size.height/2-5, self.frame.size.width-2*self.progressWidth, 10)];
        progressLabel.font = [UIFont systemFontOfSize:10];
        progressLabel.textAlignment = NSTextAlignmentCenter;
        progressLabel.backgroundColor = [UIColor clearColor];
        progressLabel.text = @"0.0%";
        [self addSubview:progressLabel];
    }
    return self;
}

//设置进度
-(void)setProgress:(float)progress animated:(BOOL)animated
{
    //设置图层的宽度
    bgLayer.lineWidth = self.progressWidth;
    progressLayer.lineWidth = self.progressWidth;
    
    //设置图层的颜色
    bgLayer.strokeColor = self.bgColor.CGColor;
    progressLayer.strokeColor = self.progressColor.CGColor;
    
    //贝赛路径
    bgPath = [UIBezierPath bezierPathWithArcCenter:CGPointMake(self.frame.size.width/2, self.frame.size.height/2) radius:(self.bounds.size.width-self.progressWidth)/2 startAngle:0 endAngle:M_PI*2 clockwise:YES];
    bgLayer.path = bgPath.CGPath;
    progressPath = [UIBezierPath bezierPathWithArcCenter:CGPointMake(self.frame.size.width/2, self.frame.size.height/2) radius:(self.bounds.size.width-self.progressWidth)/2 startAngle:- M_PI_2 endAngle:(M_PI*2)*self.progress-M_PI_2 clockwise:YES];
    progressLayer.path = progressPath.CGPath;
    
    //显示进度
    progressLabel.text = [NSString stringWithFormat:@"%.1f%%",self.progress*100];
}

-(void) animationProgress:(CAShapeLayer *)maskLayer{
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
	animation.duration  = 1;
	animation.fromValue = @0;
	animation.toValue   = @1;
    animation.delegate  = self;
	animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
	animation.removedOnCompletion = YES;
	[maskLayer addAnimation:animation forKey:@"circleAnimation"];
}

-(void) animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
