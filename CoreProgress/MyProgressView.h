//
//  MyProgressView.h
//  DynamicChart
//
//  Created by dg11185_zal on 14-10-24.
//  Copyright (c) 2014年 dg11185. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyProgressView : UIView{
    CAShapeLayer *bgLayer;//底图图层
    UIBezierPath *bgPath;//底图路径
    CAShapeLayer *progressLayer;//进度图图层
    UIBezierPath *progressPath;//进度图路径
    
    UILabel *progressLabel;//进度显示标签
}

@property(nonatomic,assign) CGFloat progress;//进度0～1之间
@property(nonatomic,assign) CGFloat progressWidth;//进度条宽度
@property(nonatomic,strong) UIColor *bgColor;//底图图层颜色
@property(nonatomic,strong) UIColor *progressColor;//进度图图层颜色
//设置进度
-(void)setProgress:(float)progress animated:(BOOL)animated;


@end
