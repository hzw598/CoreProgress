//
//  ViewController.m
//  CoreProgress
//
//  Created by dg11185_zal on 14/11/20.
//  Copyright (c) 2014年 dg11185. All rights reserved.
//

#import "ViewController.h"
#import "MyProgressView.h"

@interface ViewController (){
    MyProgressView *progressView;
    float progress;
    NSTimer *timer;
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"进度条";
    self.view.backgroundColor = [UIColor whiteColor];
    
    //当前进度
    progress = 0.0f;
    
    //进度条视图
    progressView = [[MyProgressView alloc] initWithFrame:CGRectMake(CGRectGetMidX(self.view.bounds)-50, CGRectGetMidY(self.view.bounds)-100, 100, 100)];
    progressView.backgroundColor = [UIColor whiteColor];
    progressView.bgColor = [UIColor blackColor];
    progressView.progressColor = [UIColor orangeColor];
    progressView.progress = progress;
    [self.view addSubview:progressView];
    
    //定时器，刷新进度
    timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(progressAction) userInfo:nil repeats:YES];
    
    //重置进度按钮
    UIButton *resetButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMidX(self.view.bounds)-40, CGRectGetMidY(self.view.bounds)+50, 80, 35)];
    [resetButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [resetButton setTitle:@"重新开始" forState:UIControlStateNormal];
    resetButton.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    resetButton.layer.cornerRadius = 5;
    [self.view addSubview:resetButton];
    [resetButton addTarget:self action:@selector(resetAction) forControlEvents:UIControlEventTouchUpInside];
    
}

//刷新进度
-(void) progressAction{
    progress += 0.01;
    if (progress>1.001) {
        [timer invalidate];
        return;
    }
    progressView.progress = progress;
    [progressView setProgress:progress animated:YES];
}

//重新开始
-(void) resetAction{
    progress = 0.0;
    [timer invalidate];
    timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(progressAction) userInfo:nil repeats:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
